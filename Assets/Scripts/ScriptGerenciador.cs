﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ScriptGerenciador : MonoBehaviour {
    
	public void IntroUm()
	{
		SceneManager.LoadScene("Intro01");
	}

	public void IntroDois()
	{
		SceneManager.LoadScene("Intro02");
	}

	public void IntroTres()
	{
		SceneManager.LoadScene("Intro03");
	}

    public void FaseUm()
    {
        SceneManager.LoadScene("Cena01");
    }
    public void FaseDois()
	{
		if(PlayerPrefs.HasKey("Cena02")){
			SceneManager.LoadScene("Cena02");
		}
    }
    public void FaseTres()
    {
		if(PlayerPrefs.HasKey("Cena03")){
			SceneManager.LoadScene("Cena03");
		}
    }
    public void FaseQuatro()
    {
		if(PlayerPrefs.HasKey("Cena04")){
			SceneManager.LoadScene("Cena04");
		}
    }
    public void FaseCinco()
    {
		if(PlayerPrefs.HasKey("Cena05")){
			SceneManager.LoadScene("Cena05");
		} 
    }
    public void FaseSeis()
    {
		if(PlayerPrefs.HasKey("Cena06")){
			SceneManager.LoadScene("Cena06");
		}
    }
    public void FaseSete()
    {
		if(PlayerPrefs.HasKey("Cena07")){
			SceneManager.LoadScene("Cena07");
		}
    }
    public void FaseOito()
    {
		if(PlayerPrefs.HasKey("Cena08")){
			SceneManager.LoadScene("Cena08");
		}
    }

    public void MenuFases()
    {
        SceneManager.LoadScene("MenuFases");
    }

    public void Voltar()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }

    public void Sobre()
    {
        SceneManager.LoadScene("MenuSobre");
    }
}
