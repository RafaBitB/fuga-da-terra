﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class lives : MonoBehaviour {

	public GameObject gameOverMenu;
	public GameObject controlsUI;
	public Sprite[] hearts;
	int hpDefault = 3;

	// Use this for initialization
	void Start () {
		gameOverMenu.SetActive(false);
		AlterHearts (hpDefault);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AlterHearts(int pos){
		this.GetComponent<Image>().sprite = hearts[pos];
	}

	public void LoseHp(){
		if (hpDefault >= 1) {
			hpDefault -= 1;

			if(hpDefault < 1){
				GameOverMenu ();
			}
		}
		AlterHearts(hpDefault);
	}

	void GameOverMenu(){
		gameOverMenu.SetActive(true);
		controlsUI.SetActive(false);
		Time.timeScale = 0f;
	}
}
