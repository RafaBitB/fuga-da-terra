﻿using UnityEngine;
using System.Collections;

public class ScriptRange : MonoBehaviour {
    

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnCollisionEnter2D(Collision2D player)
    {
        if(player.gameObject.tag == "Player")
        {
            this.GetComponentInChildren<Animator>().SetBool("ativo", true);
            this.transform.DetachChildren();
            Destroy(this.gameObject);
        }
    }
}
