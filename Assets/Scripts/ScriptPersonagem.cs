﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class ScriptPersonagem : MonoBehaviour
{

    Rigidbody2D playerRB;
    Animator anima;
	public AudioClip jumpSound;
	public AudioClip punshSound;
	public AudioClip hurtSound;
	private AudioSource audioS;

    public int forcaPulo;
    bool abaixado;
    bool ferido;
    bool noAr;

    public float velocidadeMovimento;
    float ladoPersonagem;
    int direcaoPersonagem;
    float colisorY;
    bool comItem;

    Vector3 originalScale;

    //Verifica se está tocando o solo;
    public GameObject confereChao;
    bool noChao;
    public LayerMask oQueEChao;
    public BoxCollider2D colisor;
	public lives live;

    void Start()
    {
        playerRB = GetComponent<Rigidbody2D>();
        anima = GetComponent<Animator>();
		audioS = GetComponent<AudioSource> ();
        ladoPersonagem = transform.localScale.x;
        colisorY = colisor.offset.y;
    }

    void Update()
    {
        if (!ferido)
        {
            Movimentar();
        }
        if (CrossPlatformInputManager.GetButtonDown("Drop"))
        {
            SoltaItem();
        }

    }
    void Para()
    {
        anima.SetBool("parado", true);
        anima.SetBool("pulo", false);
        anima.SetBool("anda", false);
        anima.SetBool("abaixado", false);
        abaixado = false;
    }
    void Anda(bool direita)
    {
        if (direita)
        {
            transform.localScale = new Vector2(ladoPersonagem * 1, transform.localScale.y);
            direcaoPersonagem = 1;
        }
        else
        {
            transform.localScale = new Vector2(ladoPersonagem * -1, transform.localScale.y);
            direcaoPersonagem = -1;
        }
        if (!noAr)
        {
            anima.SetBool("anda", true);
        }
        else
        {
            anima.SetBool("pulo", true);
        }
    }
    void Pula()
    {
        playerRB.AddForce(new Vector2(0, forcaPulo));
		audioS.clip = jumpSound;
		audioS.Play();
        anima.SetBool("anda", false);
    }
    public void Fere()
    {
        if (!ferido) {

            ferido = true;
            GetComponent<SpriteRenderer>().color = new Color(0.9f, 0.30f, 0.30f);
            anima.SetBool("ferido", ferido);
            live.LoseHp();
            audioS.clip = hurtSound;
            audioS.Play();

            if (noChao)
            {
                playerRB.AddForce(new Vector2(100 * -direcaoPersonagem, 120));
            }
            else
            {
                playerRB.AddForce(new Vector2(100 * -direcaoPersonagem, 0));
            }
            //GetComponent<Rigidbody2D>().isKinematic = true;
            Invoke("Recupera", 0.5f);
        }
    }
    void Abaixa()
    {
        anima.SetBool("abaixado", true);
        abaixado = true;
    }
    void Movimentar()
    {
        noChao = Physics2D.OverlapCircle(confereChao.transform.position, 0.2f, oQueEChao);

        if (abaixado)
        {
            colisor.offset = new Vector2 (colisor.offset.x, -colisorY);
        }
        else
        {
            colisor.offset = new Vector2(colisor.offset.x, colisorY);
        }

        if (CrossPlatformInputManager.GetAxis("Horizontal") > 0 && !abaixado)
        {
            transform.Translate((Vector2.right * velocidadeMovimento) * Time.deltaTime);
            //Direita
            Anda(true);

        }
        else if(CrossPlatformInputManager.GetAxis("Horizontal") < 0 && !abaixado)
        {
            transform.Translate((Vector2.left * velocidadeMovimento) * Time.deltaTime);
            //Esquerda
            Anda(false);

        }else if (CrossPlatformInputManager.GetAxis("Vertical") < 0 && noChao)
        {
            Abaixa();

        }
        else
        {
            Para();
        }
        //Se botão de pulo for apertado
        if (CrossPlatformInputManager.GetButtonDown("Jump") && noChao && !ferido)
        {
            Pula();
            noAr = true;
        }
        else
        {
            noAr = false;
        }
        anima.SetBool("pulo", !noChao);
    }
    void OnCollisionEnter2D(Collision2D outro)
    {
        if (outro.gameObject.tag == "coletavel" && !comItem)
        {
            originalScale = outro.transform.localScale;

            outro.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
            outro.transform.position = transform.Find("MaoE").transform.position;
            outro.transform.parent = transform.Find("MaoE").GetComponent<Transform>();
            outro.transform.localScale = originalScale/2;
            outro.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
            comItem = true;

        } else if ((outro.gameObject.tag == "inimigoImune" ) && !ferido)
        {
            if (confereChao.transform.position.y <= outro.gameObject.GetComponent<Transform>().position.y)
            {
//                outro.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
                Fere();
            }
            else
            {
//                outro.gameObject.GetComponent<Rigidbody2D>().AddForceAtPosition(new Vector2(0, 500), new Vector2(0, 0));
                //outro.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
            }

        }
        else if (outro.gameObject.tag == "inimigoPulo")
        {
            if (confereChao.transform.position.y+0.5f > outro.transform.Find("PontoFraco").gameObject.GetComponent<Transform>().position.y && !noChao)
            {
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0,400));
				audioS.clip = punshSound;
				audioS.Play();
                Destroy(outro.gameObject);
            }
            else if (confereChao.transform.position.y <= outro.gameObject.GetComponent<Transform>().position.y && !ferido)
            {
                Fere();
            }
        }
        else if (outro.gameObject.tag == "inimigoMortal")
        {
            Fere();
        }
    }
    void SoltaItem()
    {
        foreach (Transform child in transform.Find("MaoE"))
        {
            if (child.gameObject.tag == "coletavel" && comItem)
            {
                child.parent = null;
                child.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
                child.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
                StartCoroutine(AjustaTamanhoColetavel(child));
                child.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(75*direcaoPersonagem,0.5f));
                comItem = false;
            }
        }
    }
    void Recupera()
    {
        ferido = false;
        anima.SetBool("ferido", ferido);
        //GetComponent<Rigidbody2D>().isKinematic = false;
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
    }

    IEnumerator AjustaTamanhoColetavel(Transform child)
    {
        yield return new WaitForSeconds(0.25f);
        if (child != null) { child.transform.localScale = originalScale; };
    }
}