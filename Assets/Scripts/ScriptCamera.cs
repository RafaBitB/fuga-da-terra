﻿using UnityEngine;
using System.Collections;

public class ScriptCamera : MonoBehaviour {


    public Transform playerTransform;
    public Transform naveTransform;
    public Transform cameraTransform;

    // Update is called once per frame
    void Update () {
        if (playerTransform != null) { 
        cameraTransform.position = Vector3.Lerp(
            cameraTransform.position,
            new Vector3(playerTransform.position.x, cameraTransform.position.y, cameraTransform.position.z),
            1f);
        }
        else
        {
            cameraTransform.position = Vector3.Lerp(
                cameraTransform.position,
                new Vector3(naveTransform.position.x, naveTransform.position.y, cameraTransform.position.z),
                1f);
        }
    }    
}
