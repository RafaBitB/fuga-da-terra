﻿using UnityEngine;
using System.Collections;

public class ScriptAnomaliaGosma : MonoBehaviour {


    public float velocidade;
    Rigidbody2D body;
    bool ladoDireito = true;
    private float minhaPosicaoXInicial;
    public int distanciaMaxima;
    
    // Use this for initialization
	void Start () {
        body = gameObject.GetComponent<Rigidbody2D>();
        minhaPosicaoXInicial = this.gameObject.transform.position.x;
    }
	
	// Update is called once per frame
	void Update () {
        body.velocity = new Vector2(velocidade, body.velocity.y);
        if (this.gameObject.transform.position.x > (minhaPosicaoXInicial + distanciaMaxima) && ladoDireito)
        {
            Flip();
        }
        else if(this.gameObject.transform.position.x < (minhaPosicaoXInicial - distanciaMaxima) && !ladoDireito) {
            Flip();
        }
	}
    
    void Flip()
    {
        ladoDireito = !ladoDireito;
        velocidade *= -1;
        Vector3 escala = body.transform.localScale;
        escala.x *= -1;
        transform.localScale = escala;
    }
}
