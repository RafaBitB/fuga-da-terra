#Jogo 2D no estilo plataforma/educativo

Projeto de conclusão de curso de análise e desenvolvimento de sistemas.
Desenvolvido com a Game Engine Unity3D e scripts escritos em C#.

**Desenvolvido para dispositivos Android disponível na google play pelo link:**

https://play.google.com/store/apps/details?id=com.Rafael_Cardoso.A_Fuga_Da_Terra


**Próximos passos:**

Alteração das sprites;    
Otimização das funcionalidades;    
Otimização de controles virtuais;    
