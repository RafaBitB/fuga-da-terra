﻿using UnityEngine;
using System.Collections;

public class ScriptQuestions : MonoBehaviour {

	public GameObject questionCanvas;
	public GameObject controller;
	public string question;
	public string correctAnswer;
    public GameObject player;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void openQuestion(){
		questionCanvas.SetActive (true);
		Time.timeScale = 0f;
	}

	public void VerifyAnswer(string selected){
		if (selected.Equals(correctAnswer)) {
			controller.SetActive (false);
		} else {
            player.SendMessage("Fere");
		}
		Time.timeScale = 1;
		questionCanvas.SetActive (false);
	}

	void OnCollisionEnter2D(Collision2D outro){
		if (outro.gameObject.tag == "Player") {
            player = outro.gameObject;
			openQuestion ();
		}

	}
}
