﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	public static bool GameIsPaused = false;

	public GameObject pauseMenuUI;
	public GameObject controlsUI;

	// Update is called once per frame
	void Update () {
		
	}

	public void Resume(){
		pauseMenuUI.SetActive (false);
		controlsUI.SetActive(true);
		Time.timeScale = 1f;
		GameIsPaused = false;
	}

	public void Pause(){
		pauseMenuUI.SetActive (true);
		controlsUI.SetActive(false);
		Time.timeScale = 0f;
		GameIsPaused = true;
	}

	public void Retry(){
		Time.timeScale = 1f;
		controlsUI.SetActive(true);
		Scene scene = SceneManager.GetActiveScene();
		SceneManager.LoadScene(scene.name);
	}

	public void LoadMenu(){
		Time.timeScale = 1f;
		SceneManager.LoadScene ("MenuPrincipal");
	}

	public void QuitMenu(){
		Application.Quit();
	}
}
