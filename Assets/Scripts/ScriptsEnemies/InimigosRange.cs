﻿using UnityEngine;
using System.Collections;

public class InimigosRange : MonoBehaviour {

    Animator anima;
    string meuNome;

    //Verifica se está tocando o solo;
    public GameObject confereChao;
    private bool noChao;
    public LayerMask oQueEChao;
    public BoxCollider2D colisor;
    private float minhaPosicaoInicial;

    void Start () {

        anima = GetComponent<Animator>();
        meuNome = this.gameObject.name;
        minhaPosicaoInicial = this.gameObject.transform.position.y;

    }
	
	// Update is called once per frame
	void Update () {

        if (anima.GetBool("ativo"))
        {
            switch (meuNome)
            {
                case "blocoMutante": {
                        noChao = Physics2D.OverlapCircle(confereChao.transform.position, 0.1f, oQueEChao);

                        if (noChao)
                        {
                            if (this.gameObject.transform.position.y < 0)
                            {
                                this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 1.5f);
                                
                            }
                        }else if(!noChao && this.gameObject.transform.position.y > 0)
                        {
                            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -1.5f);
                        }

                    }
                    break;
                case "AnomaliaBarra":
                    {
                        
                        if (this.gameObject.transform.position.y < (minhaPosicaoInicial + 0.8f))
                        {
                            print(this.gameObject.transform.position.y);
                            this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x, this.gameObject.transform.position.y+0.1f);
                        }
                    }
                    break;
            }
        }

	}

}
