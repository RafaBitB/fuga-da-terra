﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptBossUm : MonoBehaviour {

    public float velocidade;
    Rigidbody2D body;
    bool ladoDireito = true;
    private float minhaPosicaoXInicial;
    public float distanciaMaxima;
    private int feridas;

    // Use this for initialization
    void Start()
    {
        feridas = 0;
        body = gameObject.GetComponent<Rigidbody2D>();
        minhaPosicaoXInicial = this.gameObject.transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        body.velocity = new Vector2(velocidade, body.velocity.y);
        if (this.gameObject.transform.position.x > (minhaPosicaoXInicial + distanciaMaxima) && ladoDireito)
        {
            Flip();
        }
        else if (this.gameObject.transform.position.x < (minhaPosicaoXInicial - distanciaMaxima) && !ladoDireito)
        {
            Flip();
        }
    }

    void Flip()
    {
        ladoDireito = !ladoDireito;
        velocidade *= -1;
        Vector3 escala = body.transform.localScale;
        escala.x *= -1;
        transform.localScale = escala;
    }

    void OnCollisionEnter2D(Collision2D outro)
    {
        if (outro.gameObject.name == "AcidoCloridrico")
        {
            if (feridas < 3)
            {
                transform.localScale = body.transform.localScale / 2;
                feridas++;
                velocidade *= 1.2f;
                distanciaMaxima += 1f;
                if (feridas == 3)
                {
                    this.gameObject.tag = "inimigoPulo";
                }
            }

            Destroy(outro.gameObject);
        }
    }
}
