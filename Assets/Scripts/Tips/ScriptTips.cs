﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScriptTips : MonoBehaviour {

	public Image focusedTip;
	public Canvas tipCanvas;

	// Use this for initialization
	void Start () {
		tipCanvas.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OpenTip(){
		focusedTip.sprite = this.GetComponent<SpriteRenderer> ().sprite;
		tipCanvas.gameObject.SetActive (true);
	}

	public void CloseTip(){
		tipCanvas.gameObject.SetActive (false);
	}

	void OnCollisionEnter2D(Collision2D outro){
		if (outro.gameObject.tag == "Player") {
			OpenTip ();
		}
	}
}
