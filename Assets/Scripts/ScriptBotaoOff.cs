﻿using UnityEngine;
using System.Collections;

public class ScriptBotaoOff : MonoBehaviour {

	public AudioClip activeSoud;
	public bool needCard;
	public bool needQuestion;
	private AudioSource audioS;
	private bool isActivated = false;
    public GameObject box;
	// Use this for initialization
	void Start () {
		audioS = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D outro)
    {
        if (outro.gameObject.tag == "Player")
        {
			audioS.clip = activeSoud;

			if (!isActivated) {
				if (!needCard) {
					if (!needQuestion) {
						Activate ();
					} else {
						
					}

				} else {
					if (outro.gameObject.transform.Find ("MaoE").transform.Find("Card")) {
						outro.gameObject.transform.Find ("MaoE").DetachChildren();
						Activate ();
					}
				}
			}
        }
    }
	void Activate(){
		audioS.Play ();
		this.GetComponent<Animator> ().SetBool ("ativo", true);
        if (transform.Find("elevador"))
        {
            transform.Find("elevador").SendMessage("Ligar");
        }
        else if (transform.Find("porta"))
        {
            transform.Find("porta").SendMessage("Ligar");
        }
        else {
            box.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            box.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        }
		isActivated = true;
	}
}
