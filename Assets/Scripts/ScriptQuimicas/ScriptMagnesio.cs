﻿using UnityEngine;
using System.Collections;

public class ScriptMagnesio : MonoBehaviour {

    Animator anima;

    void Start()
    {
        anima = GetComponent<Animator>();
    }

    void OnCollisionEnter2D(Collision2D outro)
    {
        if (outro.gameObject.tag == "coletavel")
        {
            anima.SetBool("reacaoQuimica", true);
            Destroy(outro.gameObject);
            StartCoroutine(Destroir());
        }
    }

    IEnumerator  Destroir()
    {
        yield return new WaitForSeconds(2);
        Destroy(this.gameObject);
    }
}
