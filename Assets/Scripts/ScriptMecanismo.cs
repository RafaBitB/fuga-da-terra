﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class ScriptMecanismo : MonoBehaviour {

    private float minhaPosicaoYinicial;
	public float taxaDeMovimento;
    public float maxTop;
    public float maxBot;
    public float variacaoZ;
    private bool ligado = false;
    private bool topo = false;
    private string meuNome;

    [SerializeField]
    private string proximaCena;

	void Start () {

        minhaPosicaoYinicial = this.gameObject.transform.position.y;
        meuNome = gameObject.name;
        
    }
	
	// Update is called once per frame
	void Update () {

        ConfereEstado();
        AlternaZ();
    }

    void Ligar()
    {
        ligado = true;
    }

    void ConfereEstado()
    {
        if (ligado)
        {
            switch (meuNome)
            {
                case "elevador":
                    {
                        if (this.gameObject.transform.position.y < minhaPosicaoYinicial + maxTop && !topo)
                        {
						this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x, this.gameObject.transform.position.y + taxaDeMovimento);
                            if (this.gameObject.transform.position.y >= minhaPosicaoYinicial + maxTop)
                            {
                                topo = true;
                            }
                        }
                        else
                        {
						this.gameObject.transform.position = new Vector2(this.gameObject.transform.position.x, this.gameObject.transform.position.y - taxaDeMovimento);
                            if (this.gameObject.transform.position.y <= minhaPosicaoYinicial - maxBot)
                            {
                                topo = false;
                            }
                        }
                    }
                    break;
                case "porta":
                    {
                        this.GetComponent<Animator>().SetBool("ativo", true);
                    }
                    break;
            }

        }
    }
    void OnTriggerEnter2D(Collider2D outro)
    {
        if(outro.gameObject.name  == "Player")
        {
            if (meuNome == "porta" && ligado)
            {
                if (outro.gameObject.tag == "Player")
                {
                    //Fase concluída
                    SceneManager.LoadScene(proximaCena);
					PlayerPrefs.SetString (proximaCena, proximaCena);
                }
            }
            else if (meuNome == "nave")
            {
                Destroy(outro.gameObject);
                this.GetComponent<Animator>().SetBool("pronta", true);
                StartCoroutine(Partir());
            }
        }
        
        
    }

    IEnumerator Partir()
    {
        yield return new WaitForSeconds(1);
        this.GetComponent<Rigidbody2D>().gravityScale = -0.1f;
        
    }

    void AlternaZ() // essa funcao tem que retonar valores entre 15 e -15
    {

        if(meuNome == "nave")
        {
            variacaoZ += 0.1f;
            if (this.GetComponent<Animator>().GetBool("pronta"))
            {
                this.gameObject.GetComponent<Transform>().transform.localRotation = transform.localRotation = Quaternion.Euler(0.0f, 0.0f, (float)Math.Sin(variacaoZ) * 15);
                if(this.gameObject.transform.position.y >= 22f)
                {
                    Fim();
                }
            }
        }
    }

    void Fim()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }

}
